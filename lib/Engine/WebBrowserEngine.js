const BaseEngine = require('./BaseEngine')
const FileSaver = require('file-saver')
const XLSX = require('xlsx-style')

module.exports = class WebBrowserEngine extends BaseEngine {

  _generateSheetFrom2DArray (data, mergeRange, columnStyle) {
    const workSheet = {}
    const range = {
      's': { 'c':10000000, 'r':10000000 },
      'e': { 'c':0, 'r':0 }
    }

    for(let row = 0; row < data.length; row++) {
      for(let col = 0; col < data[row].length; col++) {
        const cell = data[row][col];
        if(range['s']['r'] > row) {
          range['s']['r'] = row;
        }
        if(range['s']['c'] > col){
          range['s']['c'] = col;
        }
        if(range['e']['r'] < row){
          range['e']['r'] = row;	
        } 
        if(range['e']['c'] < col) {
          range['e']['c'] = col;
        }
        const cell_ref = XLSX.utils.encode_cell({'c':col,'r':row});
        workSheet[cell_ref] = cell;
      }
    }

    if(range.s.c < 10000000) {
      workSheet['!ref'] = XLSX.utils.encode_range(range)
    }

    mergeRange = mergeRange || []
    workSheet['!merges'] = mergeRange
    if( columnStyle ){
      workSheet['!cols'] = columnStyle
    }

    return workSheet
  }

  _binaryStringToArrayBuffer (binaryString) {
    const arrayBuffer = new ArrayBuffer(binaryString.length);
    const arrayBufferView = new Uint8Array(arrayBuffer);

    for (let i=0; i != binaryString.length; ++i) {
      arrayBufferView[i] = binaryString.charCodeAt(i) & 0xFF;
    }

    return arrayBuffer
  }

  _generateWorkbook (schema) {
    const workbook  = {
      'SheetNames': [],
      'Sheets' : {}
    }

    for(let sheetId=0; sheetId < schema.length; sheetId++) {
      const sheet = schema[sheetId]
      const sheetName = sheet['name']
      const dataTable = sheet['detail']['dataTable']
      const mergeList = sheet['detail']['mergeList']
      const columnStyle = sheet['detail']['columnStyle']
      const workSheet = this._generateSheetFrom2DArray(dataTable, mergeList, columnStyle)
      workbook['SheetNames'].push(sheetName)
      workbook['Sheets'][sheetName] = workSheet
    }

    return workbook
  }

  export (schema, fileName) {
    const workbook = this._generateWorkbook(schema)

    const excelWriteOptions = {
      'bookType': 'xlsx',
      'bookSST' : false,
      'type' : 'binary'
    }

    const excelBinary = XLSX.write(workbook, excelWriteOptions);
    const buffer = this._binaryStringToArrayBuffer(excelBinary);
    const contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
    FileSaver.saveAs(new Blob([buffer], {type: contentType}), fileName + '.xlsx');
  }
}