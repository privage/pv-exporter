const Sheet = require('./Utils/Sheet')
const Engine = require('./Engine')

module.exports = class Exporter {

  constructor (engine=Engine.WebBrowserEngine) {
    this.isProcessing_ = false
    this.sheetList_ = null

    this.init_()
	  this.setEngine(engine)
  }

  init_ () {
    this.sheetList_ = [];
  }

  setEngine (engine) {
    this._engine = new engine()
  }

  clearSheet () {
    return this.init_()
  }

  addSheet (sheet) {
    if (sheet instanceof Sheet) {
      this.sheetList_.push(sheet)
    } else {
      console.warn('Sheet object is required')
    }
    return this
  }

  getSchema () {
    var result = [];
    for (var sheetIdx = 0; sheetIdx < this.sheetList_.length; sheetIdx++) {
      var sheet = this.sheetList_[sheetIdx]
      result.push({
        name: sheet.getSheetName(),
        detail: sheet.getObject()
      })
    }
  
    return result
  }

  download (downloadName) {
    if (this.isProcessing_ === false) {
      this.isProcessing_ = true
      this._engine.export(this.getSchema(), downloadName)	
      this.isProcessing_ = false
    } else {
      console.warn('Export Service is not loaded yet')
    }	
  }
}