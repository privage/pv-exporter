
module.exports = class Range {
	constructor () {
		this._range = {}
	}

	setStart (row, cell) {
		this._range['s'] = { 'r' : row, 'c' : cell }

		return this
	}

	setEnd (row, cell) {
		this._range['e'] = { 'r' : row, 'c' : cell }

		return this
	}
	getObject () {
		return this._range
	}
}