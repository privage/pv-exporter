/*
styleformat = {
	numFmt: 'General',
    font: {
    	bold: true,
		sz: '11',
		color: { auto: '1', rgb: 'FFFFFF' },
		name: 'Calibri'
    },
    border: { 
      	left: { style: 'thin', color: {} },
		right: { style: 'thin', color: {} },
		top: { style: 'thin', color: {} },
		bottom: { style: 'thin', color: {} }
	},
  	alignment: { horizontal: 'center' }
	}
*/

class Style {
	constructor () {
		this._style = {}
	}

	_initFont () {
		if( !this._style.hasOwnProperty('font') ) {
			this._style['font'] = {}
		}
	}

	_initAlignment () {
		if( !this._style.hasOwnProperty('alignment') ) {
			this._style['alignment'] = {}
		}
	}

	_initBorder () {
		if( !this._style.hasOwnProperty('border') ) {
			this._style['border'] = {
				'left': {},
				'right': {},
				'top': {},
				'bottom': {}
			}
		}
	}

	setFormat (format) {
		format = format || 'General'
		this._style['numFmt'] = format

		return this
	}
	setFontFamily (fontName) {
		this._initFont()
		fontName = fontName || 'Tahoma'
		this._style['font']['name'] = fontName
	
		return this
	}

	setFontColor (argb) {
		this._initFont()
	
		if( !this._style['font'].hasOwnProperty('color') ) {
			this._style['font']['color'] = {
				'auto' : 1
			}
		}
		this._style['font']['color']['rgb'] = argb
	
		return this
	}

	bold (status) {
		if( status && typeof status !== 'boolean') {
			console.error('boolean is required')
		} else {
			status = true
		}
		this._initFont()

		this._style['font']['bold'] = status
	
		return this
	}

	underline (status) {
		if(status && typeof status !== 'boolean') {
			console.error('boolean is required')
		} else {
			status = true
		}
		this._initFont()
		this._style['font']['underline'] = status
	
		return this
	}

	italic (status) {
		if(status && typeof status !== 'boolean') {
			console.error('boolean is required')
		} else {
			status = true
		}
	
		this._initFont()
		this._style['font']['italic'] = status
	
		return this
	}

	strike (status) {
		if(status && typeof status !== 'boolean') {
			console.error('boolean is required')
		} else {
			status = true
		}
	
		this._initFont()
		this._style['font']['strike'] = status
	
		return this
	}

	outline (status) {
		if(status && typeof status !== 'boolean') {
			console.error('boolean is required')
		} else {
			status = true
		}
	
		this._initFont()
		this._style['font']['outline'] = status
	
		return this
	}

	shadow (status) {
		if( status && typeof status !== 'boolean') {
			console.error('boolean is required')
		} else {
			status = true
		}
	
		this._initFont()
		this._style['font']['shadow'] = status
	
		return this
	}

	wrapText (status) {
		if( status && typeof status !== 'boolean') {
			console.error('boolean is required')
		} else {
			status = true
		}
	
		this._initFont()
		this._style['font']['wrapText'] = status
	
		return this
	}

	setBackgroundColor (argb) {
		if( !this._style.hasOwnProperty('bgColor') ) {
			this._style['bgColor'] = {
				'auto' : 1
			}
		}
		this._style['bgColor']['rgb'] = argb
	
		return this
	}

	setHorizontal (option) {
		if( option ) {
			this._initAlignment()
			this._style['alignment']['horizontal'] = option
		} else {
			console.warn('Alignment is empty, nothing to do.')
		}
	
		return this
	}

	setVertical (option) {
		if( option ) {
			this._initAlignment()
			this._style['alignment']['vertical'] = option
		} else {
			console.warn('Alignment is empty, nothing to do.')
		}
		return this;
	}

	setBorderLeftStyle (style) {
		this._initBorder()
		this._style['border']['left']['style'] = style

		return this
	}

	setBorderTopStyle (style) {
		this._initBorder()
		this._style['border']['top']['style'] = style
	
		return this
	}

	setBorderRightStyle (style) {
		this._initBorder()
		this._style['border']['right']['style'] = style
	
		return this
	}

	setBorderBottomStyle (style) {
		this._initBorder()
		this._style['border']['bottom']['style'] = style
		return this;
	}

	setBorderLeftColor (argb) {
		this._initBorder()
	
		if(!this._style['border']['left'].hasOwnProperty('color')) {
			this._style['border']['left']['color'] = {
				'auto' : 1
			}
		}
		this._style['border']['left']['color']['rgb'] = argb
	
		return this
	}

	setBorderTopColor (argb) {
		this._initBorder()
		if( !this._style['border']['top'].hasOwnProperty('color') ) {
			this._style['border']['top']['color'] = {
				'auto' : 1
			}
		}
		this._style['border']['top']['color']['rgb'] = argb
	
		return this
	}

	setBorderRightColor (argb) {
		this._initBorder()
		if( !this._style['border']['right'].hasOwnProperty('color') ) {
			this._style['border']['right']['color'] = {
				'auto' : 1
			}
		}
		this._style['border']['right']['color']['rgb'] = argb
	
		return this
	}

	setBorderBottomColor (argb) {
		this._initBorder()
		if( !this._style['border']['bottom'].hasOwnProperty('color') ) {
			this._style['border']['bottom']['color'] = {
				'auto' : 1
			}
		}
		this._style['border']['bottom']['color']['rgb'] = argb
	
		return this
	}

	setBorderStyle (style) {
		this.setBorderLeftStyle(style)
		this.setBorderTopStyle(style)
		this.setBorderRightStyle(style)
		this.setBorderBottomStyle(style)
	
		return this
	}

	setBorderColor (argb) {
		this.setBorderLeftColor(argb)
		this.setBorderTopColor(argb)
		this.setBorderRightColor(argb)
		this.setBorderBottomColor(argb)
	
		return this
	}

	getObject () {
		return this._style;
	}
}

module.exports = Style
