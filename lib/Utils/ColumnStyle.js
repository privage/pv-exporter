const _ = require('lodash')

module.exports = class ColumnStyle {
	constructor() {
		this.style_ = {}
	}

	setMaximumDigitWidth (mdw) {
		if( _.isNumber(mdw) && !_.isNaN(mdw) ) {
			this.style_['MDW'] = mdw
		} else {
			console.warn('Integer is required.')
		}

		return this
	}

	setCharacterWidth (wch) {
		if( _.isNumber(wch) && !_.isNaN(wch) ) {
			this.style_['wch'] = wch
		} else {
			console.warn('Integer is required.')
		}

		return this;
	}

	setWidth (wpx) {
		if( _.isNumber(wpx) && !_.isNaN(wpx) ) {
			this.style_['wpx'] = wpx
		} else {
			console.warn('Integer is required.')
		}

		return this
	}

	getObject () {
		return this.style_
	}
}

	

