const STYLE = {
	'VERTICAL_ALIGNMENT' : {
		'BOTTOM' : 'bottom',
		'CENTER' : 'center',
		'TOP' : 'top'
	},
	'HORIZONTAL_ALIGNMENT' : {
		'LEFT' : 'left',
		'CENTER' : 'center',
		'RIGHT' : 'right'
	},
	'BORDER_STYLE' : {
		'THIN':'thin',
		'MEDIUM':'medium',
		'THICK':'thick',
		'DOTTED':'dotted',
		'HAIR':'hair',
		'DASHED':'dashed',
		'MEDIUMDASHED':'mediumDashed',
		'DASHDOT':'dashDot',
		'MEDIUMDASHDOT':'mediumDashDot',
		'DASHDOTDOT':'dashDotDot',
		'MEDIUMDASHDOTDOT':'mediumDashDotDot',
		'SLANTDASHDOT':'slantDashDot'
	}
}

const CELLTYPE = {
	'BOOLEAN' : 'b',
	'NUMBER' : 'n',
	'ERROR' : 'e',
	'STRING' : 's',
	'DATE' : 'd'
}

const FORMAT = {
	'GENERAL': '0', // 'General',
	'NUMBER': '1', // '0',
	'TWO_DECIMAL_NUMBER': '2', // '0.00',
	'MONEY': '3', // '#,##0',
	'MONEY_TWO_DECIMAL': '4', // '#,##0.00',
	'PERCENT': '9', // '0%',
	'TWO_DECIMAL_PERCENT': '10', // '0.00%',
	'0.00E+00': '11', // '0.00E+00',
	'# ?/?': '12', // '# ?/?',
	'# ??/??': '13', // '# ??/??',
	'm/d/yy': '14', // 'm/d/yy',
	'd-mmm-yy': '15', // 'd-mmm-yy',
	'd-mmm': '16', // 'd-mmm',
	'mmm-yy': '17', // 'mmm-yy',
	'h:mm AM/PM': '18', // 'h:mm AM/PM',
	'h:mm:ss AM/PM': '19', // 'h:mm:ss AM/PM',
	'h:mm': '20', // 'h:mm',
	'h:mm:ss': '21', // 'h:mm:ss',
	'm/d/yy h:mm': '22', // 'm/d/yy h:mm',
	'#,##0 ;(#,##0)': '37', // '#,##0 ;(#,##0)',
	'#,##0 ;[Red](#,##0)': '38', // '#,##0 ;[Red](#,##0)',
	'#,##0.00;(#,##0.00)': '39', // '#,##0.00;(#,##0.00)',
	'#,##0.00;[Red](#,##0.00)': '40', // '#,##0.00;[Red](#,##0.00)',
	'mm:ss': '45', // 'mm:ss',
	'[h]:mm:ss': '46', // '[h]:mm:ss',
	'mmss.0': '47', // 'mmss.0',
	'##0.0E+0': '48', // '##0.0E+0',
	'@': '49', // '@',
	'"上午/下午 "hh"時"mm"分"ss"秒 "': '56', // '"上午/下午 "hh"時"mm"分"ss"秒 "',
	'[$-409]d\\-mmm\\-yy;@': '167' // '[$-409]d\\-mmm\\-yy;@',
}

module.STYLE = STYLE
module.CELLTYPE = CELLTYPE
module.FORMAT = FORMAT

module.exports = {
	STYLE,
	CELLTYPE,
	FORMAT,
}