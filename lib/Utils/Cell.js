const Style = require('./Style')
const Enum = require('./Enum')

module.exports = class Cell {

    get _cellType () { return Enum.CELLTYPE }

    constructor (value, style) {
        this._style = {}
        this._value = null
        this._type = this._cellType.STRING
        if (value != null) {
            this.setValue(value)
        }
        if (style != null) {
            this.applyStyle(style)
        }
    }

    setValue(value) {
        if (this._type != null) {
            switch(typeof value) {
                case 'number':
                    this._type = this._cellType.NUMBER
                    break
                case 'string':
                    this._type = this._cellType.STRING
                    break
                case 'boolean':
                    this._type = this._cellType.BOOLEAN
                    break
                default:
                    this._type = this._cellType.STRING
            }
        }

        if (value instanceof Date) {
            value = this._convertToUTCTime(value).toISOString()
            this.setType(this._cellType.DATE)
        }
        this._value = value

        return this
    }

    setType (type) {
        this._type = type
        return this
    }

    _convertToUTCTime (date) {
        let timezoneOffset = date.getTimezoneOffset() * 60000
        return new Date(date.valueOf() - timezoneOffset)
    }

    setStyle (style) {
        this._style = {}
        return this.applyStyle(style)
    }

    applyStyle(style) {
        let listOfStyle

        if (style instanceof Array) {
            listOfStyle = style
        } else if (style instanceof Style) {
            listOfStyle = [style]
        } else {
            console.warn('Style object or Array of Style object is required.')
            return this
        }

        for (let styleId = 0; styleId < listOfStyle.length; styleId++) {
            let styleObject = listOfStyle[styleId]
            if (styleObject instanceof Style) {
                this._style = Object.assign(this._style, styleObject.getObject())
            }
        }

        return this
    }

    getObject() {
        return {
            'v': this._value,
            's': this._style,
            't': this._type
        }
    }
}