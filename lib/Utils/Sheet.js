const ColumnStyle = require('./ColumnStyle')
const Cell = require('./Cell')
const Range = require('./Range')

module.exports = class Sheet {
	constructor (sheetName) {
		this._sheetName = sheetName
		this._init()
	}

	_init () {
		this._mergeList = []
		this._columnStyles = []
		this._dataTable = []
	}

	clearData () {
		this._init()
	}


	getSheetName () {
		return this._sheetName
	}

	setSheetName (sheetName) {
		this._sheetName = sheetName
	}

	addColumnStyle (columnIndex, columnStyle) {
		if( columnStyle instanceof ColumnStyle ) {
			this._columnStyles[columnIndex] = columnStyle
		} else {
			console.warn('ColumnStyle object is required.')
		}
		return this
	}

	getBuiltColumnStyle () {
		const columnStyle = []

		for(let colIdx = 0; colIdx < this._columnStyles.length; colIdx++)  {
			const columnStyleItem = this._columnStyles[colIdx]
			if(columnStyleItem)  {
				columnStyle.push(columnStyleItem.getObject())
			} else {
				columnStyle.push({})
			}
		}
		return columnStyle
	}

	appendCell (cell) {
		if( cell instanceof Cell ) {
			this._dataTable[this._dataTable.length - 1].push(cell)
		} else {
			console.warn('Cell object is required.')
		}

		return this
	}

	addNewRow () {
		this._dataTable.push([])
		return this
	}

	getLastCellList () {
		return this._dataTable[this._dataTable.length - 1]
	}

	getBuiltDataTable () {
		const result = []
		for(let row = 0; row < this._dataTable.length; row++) {
			result.push([])
			for(let col = 0; col < this._dataTable[row].length; col++) {
				result[row].push(this._dataTable[row][col].getObject())
			}
		}

		return result
	}

	addMergeRange (range) {
		if( range instanceof Range ) {
			this._mergeList.push(range)
		} else {
			console.warn('Range object is required.')
		}

		return this
	}

	getBuiltMergeRange () {
		var mergeList = []
		for(var idx = 0; idx < this._mergeList.length; idx++)  {
			var mergeListItem = this._mergeList[idx]
			if( mergeListItem)  {
				mergeList.push(mergeListItem.getObject())
			}
		}

		return mergeList
	}

	getObject () {
		return {
			'mergeList' : this.getBuiltMergeRange(),
			'columnStyle' : this.getBuiltColumnStyle(),
			'dataTable' : this.getBuiltDataTable()
		}
	}
}