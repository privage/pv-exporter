const Exporter = require('../index')

let _style = null
let _cell = null
let _range = null
let _columnStyle = null

test('test require module', () => {

    expect(Exporter).toBeTruthy()
    expect(Exporter.Exporter).toBeTruthy()

    expect(Exporter.Engine).toBeTruthy()

    expect(Exporter.Utils).toBeTruthy()
    expect(Exporter.Utils.Cell).toBeTruthy()
    expect(Exporter.Utils.ColumnStyle).toBeTruthy()
    expect(Exporter.Utils.Enum).toBeTruthy()
    expect(Exporter.Utils.Range).toBeTruthy()
    expect(Exporter.Utils.Sheet).toBeTruthy()
    expect(Exporter.Utils.Style).toBeTruthy()
})

test('test create Style', () => {
    const Style = Exporter.Utils.Style
    const Enum = Exporter.Utils.Enum

    const style = new Style()
    style.setFormat(Enum.FORMAT.GENERAL)
        .setFontFamily('Calibri')
        .setFontColor('FF0000') // red color
        .setHorizontal(Enum.STYLE.HORIZONTAL_ALIGNMENT.CENTER)
        .setVertical(Enum.STYLE.VERTICAL_ALIGNMENT.CENTER)
        .bold(true)
        .underline(true)
        .italic(true)
        .strike(true)
        .outline(true)
        .shadow(true)
        .wrapText(true)
        .setBackgroundColor('000000') // black color
        .setBorderLeftStyle(Enum.STYLE.BORDER_STYLE.THIN)
        .setBorderTopStyle(Enum.STYLE.BORDER_STYLE.THIN)
        .setBorderRightStyle(Enum.STYLE.BORDER_STYLE.THIN)
        .setBorderBottomStyle(Enum.STYLE.BORDER_STYLE.THIN)
        .setBorderLeftColor('00FF00') // green color
        .setBorderTopColor('00FF00') // green color
        .setBorderRightColor('00FF00') // green color
        .setBorderBottomColor('00FF00') // green color

    const result = style.getObject()
    const expectedResult = {
        numFmt: Enum.FORMAT.GENERAL,
        font: {
            bold: true,
            color: { auto: 1, rgb: 'FF0000' },
            name: 'Calibri',
            wrapText: true,
            underline: true,
            italic: true,
            strike: true,
            outline: true,
            shadow: true,
        },
        border: { 
            left: {
                style: Enum.STYLE.BORDER_STYLE.THIN,
                color: {
                    auto: 1,
                    rgb: '00FF00'
                }
            },
            right: {
                style: Enum.STYLE.BORDER_STYLE.THIN,
                color: {
                    auto: 1,
                    rgb: '00FF00'
                }
            },
            top: {
                style: Enum.STYLE.BORDER_STYLE.THIN,
                color: {
                    auto: 1,
                    rgb: '00FF00'
                },
            },
            bottom: {
                style: Enum.STYLE.BORDER_STYLE.THIN,
                color: {
                    auto: 1,
                    rgb: '00FF00'
                },
            },
        },
        bgColor: {
            auto: 1,
            rgb: '000000',
        },
        alignment: {
            horizontal: Enum.STYLE.HORIZONTAL_ALIGNMENT.CENTER,
            vertical: Enum.STYLE.VERTICAL_ALIGNMENT.CENTER
        }
    }
    expect(result).toEqual(expectedResult)
    _style = style
})

test('test create Cell', () => {
    const Cell = Exporter.Utils.Cell
    const Enum = Exporter.Utils.Enum

    const cell = new Cell()
    cell.setValue('Example')
    cell.setStyle(_style)
    cell.applyStyle(_style)
    cell.setType(Enum.CELLTYPE.BOOLEAN)

    const result = cell.getObject()
    const expectedResult = {
        v: 'Example',
        s: _style.getObject(),
        t: Enum.CELLTYPE.BOOLEAN,
    }
    expect(result).toEqual(expectedResult)

    _cell = cell
})

test('test create Range', () => {
    const Range = Exporter.Utils.Range
    const rowNumber = 0
    const columnNumber = 0

    const range = new Range()
    range.setStart(rowNumber, columnNumber )
    range.setEnd(rowNumber + 1, columnNumber + 1)

    _range = range
})

test('test create ColumnStyle', () => {
    const ColumnStyle = Exporter.Utils.ColumnStyle

    const columnStyle = new ColumnStyle()
    columnStyle.setWidth(16)
    columnStyle.setCharacterWidth(128)
    columnStyle.setMaximumDigitWidth(6)

    _columnStyle = columnStyle
})

test('test create Sheet', () => {
    const Sheet = Exporter.Utils.Sheet
    const Cell = Exporter.Utils.Cell

    const blankCell = new Cell()

    const sheet = new Sheet()
    sheet.setSheetName('Sheet1')

    sheet.addNewRow()
    sheet.appendCell(_cell)
    sheet.appendCell(blankCell)

    sheet.addNewRow()
    sheet.appendCell(blankCell)
    sheet.appendCell(blankCell)

    sheet.addMergeRange(_range)

    const columnOne = 0
    const columnTwo = 1
    sheet.addColumnStyle(columnOne, _columnStyle)
    sheet.addColumnStyle(columnTwo, _columnStyle)

    const exporter = new Exporter.Exporter()
    exporter.setEngine
    exporter.addSheet(sheet)
    const fileName = 'example'
    exporter.download(fileName)
})
