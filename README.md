# Installation #
To install this package, go to package.json and then add

``` "pv-exporter": "git+ssh://git@bitbucket.org:privage/excelexporter.git#latest" ```

to the dependencies

# Using with WebPack #
add this code to webpack config to avoid webpack error

```
externals: [
    { './cptable': 'var cptable' }
]
```

# Code Example #

```
const Exporter = require('pv-exporter')

const Sheet = Exporter.Utils.Sheet
const Cell = Exporter.Utils.Cell
const Enum = Exporter.Utils.Enum
const Style = Exporter.Utils.Style
const ColumnStyle = Exporter.Utils.ColumnStyle
const Range = Exporter.Utils.Range

// create cell style
const style = new Style()
style.setFormat(Enum.FORMAT.GENERAL)
.setFontFamily('Calibri')
.setFontColor('FFFF0000') // red color in ARGB format
.setHorizontal(Enum.STYLE.HORIZONTAL_ALIGNMENT.CENTER)
.setVertical(Enum.STYLE.VERTICAL_ALIGNMENT.CENTER)
.bold(true)
.underline(true)
.italic(true)
.strike(true)
.outline(true)
.shadow(true)
.wrapText(true)
.setBackgroundColor('FF000000') // black color in ARGB format
.setBorderLeftStyle(Enum.STYLE.BORDER_STYLE.THIN)
.setBorderTopStyle(Enum.STYLE.BORDER_STYLE.THIN)
.setBorderRightStyle(Enum.STYLE.BORDER_STYLE.THIN)
.setBorderBottomStyle(Enum.STYLE.BORDER_STYLE.THIN)
.setBorderLeftColor('FF00FF00') // green color in ARGB format
.setBorderTopColor('FF00FF00') // green color in ARGB format
.setBorderRightColor('FF00FF00') // green color in ARGB format
.setBorderBottomColor('FF00FF00') // green color in ARGB format

const borderLeftStyle = new Style()
borderLeftStyle
    .setBorderLeftStyle(Enum.STYLE.BORDER_STYLE.THIN)
    .setBorderLeftColor('FF00FF00') // green color in ARGB format

const borderRightStyle = new Style()
borderRightStyle
    .setBorderRightStyle(Enum.STYLE.BORDER_STYLE.THIN)
    .setBorderRightColor('FF00FF00') // green color in ARGB format

// create cell
const blankCell = new Cell('')

// cell with style
const cell = new Cell()
cell.setValue('ทดสอบ')
cell.setStyle(style) // the setStyle method will replace the previous style of cell
cell.setType(Enum.CELLTYPE.STRING)

const unicodeCell = new Cell('ไทย')

const booleanCell = new Cell(true)
booleanCell.setType(Enum.CELLTYPE.BOOLEAN)

// if the value is date object,
// it will auto set cell type to DATE.
const dateCell = new Cell(new Date())

const multiAppliedStyleCell = new Cell()
multiAppliedStyleCell.applyStyle(borderLeftStyle)
multiAppliedStyleCell.applyStyle(borderRightStyle)
// or
multiAppliedStyleCell.applyStyle([borderRightStyle, borderLeftStyle])

// create sheet
const sheet = new Sheet()
sheet.setSheetName('ทดสอบ')

sheet.addNewRow()
sheet.appendCell(cell)
sheet.appendCell(blankCell)
sheet.appendCell(unicodeCell)

sheet.addNewRow()
sheet.appendCell(booleanCell)
sheet.appendCell(dateCell)
sheet.appendCell(multiAppliedStyleCell)

// create range for merge
const rowNumber = 3
const columnNumber = 0

const range = new Range()
range.setStart(rowNumber, columnNumber )
range.setEnd(rowNumber + 1, columnNumber + 1)
sheet.addMergeRange(range)


// create column style
const columnStyle = new ColumnStyle()
columnStyle.setWidth(16)
columnStyle.setCharacterWidth(128)
columnStyle.setMaximumDigitWidth(6)

const columnOne = 0
const columnTwo = 1
sheet.addColumnStyle(columnOne, columnStyle)
sheet.addColumnStyle(columnTwo, columnStyle)

// add sheet and download
const exporter = new Exporter.Exporter()
exporter.setEngine
exporter.addSheet(sheet)
const fileName = 'example'
exporter.download(fileName)
```